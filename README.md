
# Documentation

### wordpress push

 This service allows the connection of the user's device with a wordpress page.

### Requirements for use:

 - Installed and configured the push notification service in King of app.
 - You must have the King of app plugin for wordpress installed on your website.
 - The wordpress plugin must have the configuration complete with the firebase server key.
 
 ### Setting:
 
 This service only requires configuring the domain of the wordpress that you want to link.
 To do this, write the domain of your website in the "Page" field in the service configuration.
 