# Documentación

### wordpress push 

 Este servicio permite la conexión del dispositivo del usuario con una página de wordpress.

### Requisitos de uso:

 - Instalado y configurado el servicio de notificaciones push en King of app.
 - Debe tener instalado en su web el plugin de King of app para wordpress.
 - El plugin en wordpress debe tener completa la configuración con la clave del servidor de firebase.
 
 ### Configuración:
 
 Este servicio solamente requiere configurar el dominio del wordpress que desea vincular.
 Para realizarlo escriba el dominio de su web en el campo "Page" en la configuración del servicio.