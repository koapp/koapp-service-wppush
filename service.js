(function () {
  angular
    .module('king.services.wppush', [])
    .run(loadFunction);

  loadFunction.$inject = ['configService', '$http'];

  function loadFunction(configService, $http) {
    // Register upper level modules
    try {
      if (configService.services && configService.services.wppush) {
        sendDataToWp($http, configService.services.wppush.scope);
      } else {
        throw "The service is not added to the application";
      }
    } catch (error) {
      console.error("Error", error);
    }
  }

  function sendDataToWp($http, scopeData) {
  
    var checkLoginLoop = setInterval(sendDataToWp , 1000 * 10);
    
    function sendDataToWp(){
        
        console.log("Sending code to wordpress");
        if( !localStorage.getItem('deviceToken') || localStorage.getItem('deviceToken') === "") return;
        
        let url = `${scopeData.wppage}/kingofapp/?rest_route=/koapush/v1/push_code&push_code=${localStorage.getItem('deviceToken')}`;

        // Simple GET request example:
        $http({
          method: 'GET',
          url: url
        }).then(function successCallback(response) {
            console.log(response);
            if(JSON.parse(response.data).error === false){
                clearInterval(checkLoginLoop);
            }
          }, function errorCallback(response) {
            console.log(response);
          });
    }

    
   sendDataToWp()
  
  }

  // --- End servicenameController content ---
})();